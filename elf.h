///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 05b - Readelf
///
/// @file elf.h
/// @version 1.0
//
/// @author Joseph Paragas <joseph60@hawaii.edu>
/// @brief  Lab 05b - Readelf - EE 491F - Spr 2021
/// @date   22_Feb_2021
/// @info   This is the header file for file "elf.c"  
//            
///////////////////////////////////////////////////////////////////////////////////
/* Define the object file type */

#define ET_NONE         0     /* No file type */
#define ET_REL          1     /* Relocatable file */
#define ET_EXEC         2     /* Executable file */
#define ET_DYN          3     /* Shared object file */
#define ET_CORE         4     /* Core file */
#define ET_LOPROC   0xff0     /* Processor-specific */
#define ET_HIPROC   0xfff     /* Processor-specific*/


/* Define the required architecture for a file */
#define EM_NONE         0     /* No machine */
#define EM_M32          1     /* AT&T WE 32100 */
#define EM_SPARC        2     /* SPARC */
#define EM_386          3     /* Intel 80386 */
#define EM_68K          4     /* Motorolla 68000 */
#define EM_88K          5     /* Motorolla 88000 */
#define EM_860          7     /* Intel 80860 */
#define EM_MIPS         8     /* MIPS RS3000 */


/* Define the OS and ABI where the object is targeted */
#define ELFOSABI_SYSV              0   /* ELFOSABI_NONE is also 0 */
#define ELFOSABI_HPUX              1
#define ELFOSABI_NETBSD            2
#define ELFOSABI_LINUX             3 
#define ELFOSABI_SOLARIS           6
#define ELFOSABI_IRIX              8  
#define ELFOSABI_FREEBSD           9
#define ELFOSABI_TRU64            10
#define ELFOSABI_ARM              97
#define ELFOSABI_STANDALONE      255

/* sh types */
#define SHT_NULL                   0
#define SHT_PROGBITS               1
#define SHT_SYMTAB                 2
#define SHT_STRTAB                 3
#define SHT_RELA                   4
#define SHT_HASH                   5
#define SHT_DYNAMIC                6
#define SHT_NOTE                   7

/* sh flags */
#define SHF_WRITE                       0x1
#define SHF_ALLOC                       0x2
#define SHF_EXECINSTR                  0x4
#define SHF_MASKPROC             0xf0000000
