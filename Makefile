#///////////////////////////////////////////////////////////////////////////////
#/// University of Hawaii, College of Engineering
#/// EE 491 - Software Reverse Engineering
#/// Lab 05b - Readelf
#///
#/// @file elf.c
#/// @version 1.0
#//
#/// @author Joseph Paragas <joseph60@hawaii.edu>
#/// @brief  Lab 05b - Readelf - EE 491F - Spr 2021
#/// @date   22_Feb_2021
#/// @info   This is the Makefile for Lab05b - Readelf
#//
#///////////////////////////////////////////////////////////////////////////////////
CC     = gcc
CFLAGS = -g -Wall

TARGET = readelf

all: $(TARGET)

readelf: elf.c
	$(CC) $(CFLAGS) -o $(TARGET) elf.c 

clean: 
	rm $(TARGET)

test:
	./$(TARGET) -h $(TARGET)
	./$(TARGET) -c $(TARGET)
