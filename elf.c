///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 05b - Readelf
///
/// @file elf.c
/// @version 1.0
//
/// @author Joseph Paragas <joseph60@hawaii.edu>
/// @brief  Lab 05b - Readelf - EE 491F - Spr 2021
/// @date   22_Feb_2021
/// @info   This file contains the program that reproduces the functionality of the 
//          "readelf -h" command   
//            
///////////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "elf.h"

#define EI_NIDENT 16

const char *shdr_get_type(uint64_t);
const char *shdr_get_flags(uint64_t);
///////////////////////////////////////////////////////////////////////////////////
/* 64-bit ELF base types */
typedef unsigned long long  Elf64_Addr;
typedef unsigned long long  Elf64_Off;
typedef unsigned short      Elf64_Half;
typedef unsigned int        Elf64_Word;

/* 64-bit ELF header */
typedef struct{
   unsigned char           e_ident[EI_NIDENT];
   Elf64_Half              e_type;
   Elf64_Half              e_machine;
   Elf64_Word              e_version;
   Elf64_Addr              e_entry;
   Elf64_Off               e_phoff;
   Elf64_Off               e_shoff;
   Elf64_Word              e_flags;
   Elf64_Half              e_ehsize;
   Elf64_Half              e_phentsize;
   Elf64_Half              e_phnum;
   Elf64_Half              e_shentsize;
   Elf64_Half              e_shnum;
   Elf64_Half              e_shstrndx;
} Elf64_Ehdr;

typedef struct {
    uint32_t   sh_name;
    uint32_t   sh_type;
    uint64_t   sh_flags;
    Elf64_Addr sh_addr;
    Elf64_Off  sh_offset;
    uint64_t   sh_size;
    uint32_t   sh_link;
    uint32_t   sh_info;
    uint64_t   sh_addralign;
    uint64_t   sh_entsize;
} Elf64_Shdr;

Elf64_Ehdr header;
Elf64_Shdr s_header;

/* Read the contents of the elf file */
void readfile(char* filename, Elf64_Ehdr* header)
{
   FILE* file;

   file = fopen(filename, "rb");

   if (file == NULL)
   {
      fprintf(stderr, "ERROR: Cannot open %s\n", filename);
      exit(EXIT_FAILURE);  
   }

   fread(header, sizeof(Elf64_Ehdr), 1, file);
   

}

void readfile2(char* filename, Elf64_Shdr* s_header)
{
   FILE* file;

   file = fopen(filename, "rb");

   if (file == NULL)
   {
      fprintf(stderr, "ERROR: Cannot open %s\n", filename);
      exit(EXIT_FAILURE);
   }

   fread(s_header, sizeof(Elf64_Shdr), 1, file);

}

void print_info(Elf64_Ehdr* header)
{

   int i;
   printf("Elf Header: \n");
   printf(" Magic: ");
   
   for (i = 0; i < 16; i++)
   {
      if (header->e_ident[i] > 0xe)
      {
         printf("%x", header->e_ident[i]);
         printf(" ");
      }

      else 
      {
         printf("0%x", header->e_ident[i]);
         printf(" ");
      }
   }

   printf("\n");

   printf(" Class: \t\t\t\t\t");

   for (i = 0; i < 3; i++)
   {
      printf("%c", header->e_ident[i + 1]);
   }

   switch(header->e_ident[4])
   {
      case 0:
         printf("Invalid class \n");
         break;

      case 1: 
         printf("32\n");
         break;

      case 2: 
         printf("64\n");
         break;
   }

   printf(" Data: \t\t\t\t\t\t");

   switch(header->e_ident[5])
   {
      case 0:
         printf("Invalid data encoding \n");
         break;
      case 1: 
         printf("2's compliment, little endian\n");
         break;
      case 2: 
         printf("2's compliment, big endian\n");
   }

   printf(" Version: \t\t\t\t\t");

   switch(header->e_ident[6])
   {
      case 0:
         printf("%d (invalid)\n" , header->e_version);
         break;
      case 1: 
         printf("%d (current)\n" , header->e_version);
   }

   printf(" OS/ABI: \t\t\t\t\t");

   switch(header->e_ident[7])
   {
      case ELFOSABI_SYSV:
         printf("UNIX - System V \n");
         break;

      case ELFOSABI_HPUX:
         printf("HP-UX \n");
         break;

      case ELFOSABI_NETBSD:
         printf("NetBSD \n");
         break;

      case ELFOSABI_LINUX:
         printf("Linux \n");
         break;

      case ELFOSABI_SOLARIS: 
         printf("Solaris \n");
         break;

      case ELFOSABI_IRIX: 
         printf("IRIX \n");
         break;

      case ELFOSABI_FREEBSD:
         printf("FreeBSD \n");
         break;

      case ELFOSABI_TRU64: 
         printf("TRU64 Unix \n");
         break;

      case ELFOSABI_ARM: 
         printf("ARM architecture \n");
         break;

      case ELFOSABI_STANDALONE:
         printf("Stand-alone (embedded) \n");
         break;
      
      /* Prints other for an OS/ABI that was not mentioned above */
      default: 
         printf("Other \n");
         break;

   }

   printf(" ABI Version: \t\t\t\t\t");

   switch(header->e_ident[8])
   {
      case 0: 
         printf("0 \n");
         break;

      case 1:
         printf("1 \n");
         break;
   }

   printf(" Type: \t\t\t\t\t\t");
      
   switch(header->e_type)
   {
      case ET_NONE: 
         printf("No file type \n");
         break;

      case ET_REL:
         printf("Relocatable file \n");
         break;

      case ET_EXEC: 
         printf("EXEC (Executable file) \n");
         break;

      case ET_DYN:
         printf("Shared object file \n");
         break;

      case ET_CORE: 
         printf("Core file \n");
         break;

      case ET_LOPROC:
         printf("Processor-specific \n");
         break;

      case ET_HIPROC:
         printf("Processor-specific \n");
         break;

   } 

   printf(" Machine: \t\t\t\t\t");

   switch(header->e_machine)
   {

      case EM_NONE:
         printf("No machine \n");
         break;
         
      case EM_M32: 
         printf("AT&T WE 32100 \n");
         break;

      case EM_SPARC:
         printf("SPARC \n");
         break;

      case EM_386:
         printf("Intel 80386 \n");
         break;

      case EM_68K:
         printf("Motorolla 68000 \n");
         break;

      case EM_88K: 
         printf("Motorolla 88000 \n");
         break;

      case EM_860: 
         printf("Intel 80860 \n");
         break; 

      case EM_MIPS: 
         printf("MIPS RS3000 \n");
         break;
         
      default:
         printf("Other \n");
         break;
   }

   printf(" Version: \t\t\t\t\t");

   switch(header->e_version)
   {
      case 0:
         printf("0x0 \n");
         break;

      case 1: 
         printf("0x1 \n");
         break;
   }

   printf(" Entry point address: \t\t\t\t0x%llx\n" , header->e_entry);
   printf(" Start of program headers: \t\t\t%lld (bytes into file)\n" , header->e_phoff);
   printf(" Start of section headers: \t\t\t%lld (bytes into file)\n" , header->e_shoff);
   printf(" Flags: \t\t\t\t\t0x%x\n", header->e_flags);
   printf(" Size of this header: \t\t\t\t%d (bytes)\n" , header->e_ehsize);
   printf(" Size of program headers: \t\t\t%d (bytes)\n" , header->e_phentsize);
   printf(" Number of program headers: \t\t\t%d\n" , header->e_phnum);
   printf(" Size of section headers: \t\t\t%d (bytes)\n" , header->e_shentsize);
   printf(" Number of section headers: \t\t\t%d\n" , header->e_shnum);
   printf(" Section header string table index:\t\t%d\n", header->e_shstrndx);

}

const char *shdr_get_type(uint64_t sh_type)
{
    switch (sh_type) {
        case SHT_NULL: 
           return "NULL"; 
           break;
        case SHT_PROGBITS: 
           return "PROGBITS"; 
           break;
        case SHT_SYMTAB: 
           return "SYMTAB"; 
           break;
        case SHT_STRTAB: 
           return "STRTAB"; 
           break;
        case SHT_RELA: 
           return "RELA"; 
           break;
        case SHT_HASH: 
           return "HASH"; 
           break;
        case SHT_DYNAMIC: 
           return "DYNAMIC"; 
           break;
        case SHT_NOTE: 
           return "NOTE"; 
           break;
        default: 
           return ""; 
           break;
    }
}

const char *shdr_get_flags(uint64_t sh_flags)
{
    switch (sh_flags) {
        case SHF_WRITE: 
           return "W"; 
           break;
        case SHF_ALLOC: 
           return "A"; 
           break;
        case SHF_EXECINSTR: 
           return "X"; 
           break;
        case SHF_MASKPROC: 
           return "M"; 
           break;
        default: 
           return ""; 
           break;
    }

}

void print_section_header(Elf64_Ehdr* ehdr, Elf64_Shdr* shdr)
{

   printf("  %-23s%-17s%-18s%s\n", "[Nr] Name", "Type", "Address", "Offset");
   printf("       %-18s%-17s%s\n", "Size", "EntSize", "Flags  Link  Info  Align");

   for (int i = 0; i < ehdr->e_shnum; i++)
   {

      Elf64_Shdr* iter = &shdr[i];
      printf(" [%*d] %-16s %-16s %016llu %08llu\n", 2, i, "section_name", shdr_get_type(iter->sh_type), iter->sh_addr, iter->sh_offset);
      printf(" %016lx %016lx %-9s%-6d%-6d%ld\n", iter->sh_size, iter->sh_entsize, shdr_get_flags(iter->sh_flags), iter->sh_link, iter->sh_info, iter->sh_addralign); 
   
   }
   
}

int main(int argc, char *argv[])
{
   if (strcmp(argv[1], "-h") == 0)
   {

      readfile(argv[2], &header);
      print_info(&header);
      return 0;

   }

   else if(strcmp(argv[1], "-t") == 0)
   {
   
      readfile2(argv[2], &s_header);
      print_section_header(&header, &s_header);
      return 0;

   }

   else 
   {
      printf("Cannot run [%s] (invalid command)\n", argv[0]);
      exit(EXIT_FAILURE);
   }

}


